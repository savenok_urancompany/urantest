//
//  GameConstants.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

struct PhysicsCategory {
    static let None      : UInt32 = 0
    static let All       : UInt32 = UInt32.max
    static let Projectile   : UInt32 = 0b1       // 1
    static let Player: UInt32 = 0b10      // 2
    static let BorderTopBottom: UInt32 = 0b100      // 4
    static let BorderLeft: UInt32 = 0b1000      // 8
    static let BorderRight: UInt32 = 0b10000      // 16
}

struct GameConstants {
    
    struct GameObjects {
        struct Ball {
            static let width = 20.0
            static let height = 20.0
            static let texture = "Crystal_Ball"
        }
        struct Raquet {
            static let width = 20.0
            static let height = 70.0
            static let texture = "Raquet_Texture"
        }
        struct Border {
            static let width = 1.0
            static let texture = "Border_Texture"
        }
        
        struct Labels {
            static let font = "Arial"
            static let fontSize:CGFloat = 25.0
            static let color = SKColor.blue
            static let textTopOffset:CGFloat = 80.0
            static let scoreTopOffset:CGFloat = 50.0
        }
        
    }
    static let gameSpeedMaxX = 250
    static let gameSpeedMinX = 100
    static let gameSpeedMaxY = 50
    static let gameSpeedMinY = 10
}
