//
//  GameLogic.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

enum CollisionType {
    case None
    case Raquet
    case BorderTopBottom
    case BorderLeft
    case BorderRight
}

enum PlayerType {
    case playerLeft
    case playerRight
}

enum GameState {
    case start
    case playing
    case scored
}

class GameLogic: UIInteraction {
 
    weak var scene:SKScene?
    var selectedNode = SKSpriteNode()
    
    var borders: [GameObjectBorder]? = nil
    
    var centralBorder: SKSpriteNode? = nil
    
    var ball: GameObjectBall? = nil
    var leftRaquet: GameObjectRaquet? = nil
    var rightRaquet: GameObjectRaquet? = nil
    
    var textLabel: SKLabelNode? = nil
    var scoreLabel: SKLabelNode? = nil
    
    var gameState: GameState = GameState.start
    
    var leftPlayerScore = 0
    var rightPlayerScore = 0
    
    var size: CGSize = CGSize(width: 0, height: 0)
    
    var selectedPlayer: PlayerType = PlayerType.playerLeft
    
    func sceneInitialSetup(size: CGSize, scene: SKScene) {
        
        self.size = size
        self.scene = scene
        self.bordersInitialSetup()
        self.raquetsAndBallInitialSetup()
        self.labelsInitialSetup()
        self.initialPositions()
    }
    
    func bordersInitialSetup() {
        
        let topBorder = GameObjectBorder(size: CGSize(width: Double(self.size.width),
                                                      height: GameConstants.GameObjects.Border.width),
                                         textureName: GameConstants.GameObjects.Border.texture,
                                         collisionType: PhysicsCategory.BorderTopBottom)
        
        let bottomBorder = GameObjectBorder(size: CGSize(width: Double(self.size.width),
                                                         height: GameConstants.GameObjects.Border.width),
                                            textureName: GameConstants.GameObjects.Border.texture,
                                            collisionType: PhysicsCategory.BorderTopBottom)
        
        let leftBorder = GameObjectBorder(size: CGSize(width: GameConstants.GameObjects.Border.width,
                                                       height: Double(self.size.height)),
                                          textureName: GameConstants.GameObjects.Border.texture,
                                          collisionType: PhysicsCategory.BorderLeft)
        
        let rightBorder = GameObjectBorder(size: CGSize(width: GameConstants.GameObjects.Border.width,
                                                        height: Double(self.size.height)),
                                           textureName: GameConstants.GameObjects.Border.texture,
                                           collisionType: PhysicsCategory.BorderRight)
        
        self.centralBorder = SKSpriteNode(color: UIColor.orange, size: CGSize(width: 1.0, height: self.size.height))
        self.centralBorder?.position = CGPoint(x: self.size.width/2.0, y: self.size.height/2)
        
        topBorder.position = CGPoint(x: self.size.width/2.0, y: self.size.height)
        bottomBorder.position = CGPoint(x: self.size.width/2.0, y: 0.0)
        leftBorder.position = CGPoint(x: 0.0, y: self.size.height/2)
        rightBorder.position = CGPoint(x: self.size.width, y: self.size.height/2)
        
        self.borders = [topBorder, bottomBorder, leftBorder, rightBorder]
    }
    
    func raquetsAndBallInitialSetup() {
        
        self.ball = GameObjectBall(size: CGSize(width: GameConstants.GameObjects.Ball.width,
                                                height: GameConstants.GameObjects.Ball.height),
                                   textureName: GameConstants.GameObjects.Ball.texture)
        self.leftRaquet = GameObjectRaquet(size: CGSize(width: GameConstants.GameObjects.Raquet.width,
                                                        height: GameConstants.GameObjects.Raquet.height),
                                           textureName: GameConstants.GameObjects.Raquet.texture)
        
        self.rightRaquet = GameObjectRaquet(size: CGSize(width: GameConstants.GameObjects.Raquet.width,
                                                         height: GameConstants.GameObjects.Raquet.height),
                                            textureName: GameConstants.GameObjects.Raquet.texture)
    }
    
    func labelsInitialSetup() {
        
        self.textLabel = SKLabelNode(fontNamed: "Arial")
        self.textLabel?.position = CGPoint(x: self.size.width/2.0, y: self.size.height - GameConstants.GameObjects.Labels.textTopOffset)
        self.textLabel?.fontSize = GameConstants.GameObjects.Labels.fontSize
        self.textLabel?.fontColor = SKColor.blue
        self.textLabel?.text = "Tap to start"
        
        self.scoreLabel = SKLabelNode(fontNamed: "Arial")
        self.scoreLabel?.position = CGPoint(x: self.size.width/2.0, y: self.size.height - GameConstants.GameObjects.Labels.scoreTopOffset)
        self.scoreLabel?.fontSize = GameConstants.GameObjects.Labels.fontSize
        self.scoreLabel?.fontColor = GameConstants.GameObjects.Labels.color
        
    }
    
    func initialPositions() {
        
        self.ball?.position = CGPoint(x: self.size.width/2.0, y: self.size.height/2)
        self.leftRaquet?.position = CGPoint(x: (self.leftRaquet?.size.width)!, y: size.height * 0.5)
        self.rightRaquet?.position = CGPoint(x: self.size.width - (self.rightRaquet?.size.width)!, y: size.height * 0.5)
    }
    
    func startBall() {
        self.gameState = GameState.playing
        let dx =  random(min: CGFloat(GameConstants.gameSpeedMinX), max: CGFloat(GameConstants.gameSpeedMaxX))
        let dy =  random(min: CGFloat(GameConstants.gameSpeedMinY), max: CGFloat(GameConstants.gameSpeedMaxY))
        self.ball?.physicsBody?.velocity = CGVector(dx: dx, dy: dy)
    }
    
    func panForTranslation(translation: CGPoint) {
        let playerRaquet: GameObjectRaquet
        switch self.selectedPlayer {
        case PlayerType.playerRight:
            playerRaquet = self.rightRaquet!
        default:
            playerRaquet = self.leftRaquet!
        }
        
        let position = playerRaquet.position
        let topThreshold = self.size.height - playerRaquet.size.height/2
        let bottomThreshold = 0 + playerRaquet.size.height/2
        
        if position.y + translation.y < bottomThreshold {
            playerRaquet.position = CGPoint(x: position.x, y: bottomThreshold)
            return
        }
        
        if position.y + translation.y > topThreshold {
            playerRaquet.position = CGPoint(x: position.x, y: topThreshold)
            return
        }
        
        playerRaquet.position = CGPoint(x: position.x, y: position.y + translation.y)
    }
    
    func processContact(bodyA: SKPhysicsBody, bodyB: SKPhysicsBody) -> CollisionType {
        
        if bodyA.collisionBitMask != PhysicsCategory.Projectile {
            return CollisionType.None
        }
        
        switch bodyB.collisionBitMask {
        case PhysicsCategory.Player:
            return CollisionType.Raquet
        case PhysicsCategory.BorderTopBottom:
            return CollisionType.BorderTopBottom
        case PhysicsCategory.BorderLeft:
            return CollisionType.BorderLeft
        case PhysicsCategory.BorderRight:
            return CollisionType.BorderRight
        default:
            return CollisionType.None
        }
        
    }
    
    func processCollision(collision: CollisionType) {
        
        switch collision {
        case CollisionType.None:
            return
        case CollisionType.Raquet:
            self.ball?.physicsBody?.velocity = self.mirrorVelocity(velocity: (self.ball?.physicsBody?.velocity)!,
                                                                   horizontal: true)
        case CollisionType.BorderTopBottom:
            self.ball?.physicsBody?.velocity = self.mirrorVelocity(velocity: (self.ball?.physicsBody?.velocity)!,
                                                                   horizontal: false)
        case CollisionType.BorderLeft:
            self.scoreGoal(player: PlayerType.playerRight)
        case CollisionType.BorderRight:
            self.scoreGoal(player: PlayerType.playerLeft)
        }
        
    }
    
    func mirrorVelocity(velocity: CGVector, horizontal: Bool) -> CGVector {
        
        if (horizontal) {
            let horizontalVelocity = -1 * velocity.dx
            let verticalVelocity = velocity.dy
            return CGVector(dx: horizontalVelocity,dy: verticalVelocity)
        } else {
            let horizontalVelocity = velocity.dx
            let verticalVelocity = -1 * velocity.dy
            return CGVector(dx: horizontalVelocity,dy: verticalVelocity)
        }
    }
    
    func scoreGoal(player: PlayerType) {
        
        switch player {
        case PlayerType.playerLeft:
            self.leftPlayerScore += 1
        case PlayerType.playerRight:
            self.rightPlayerScore += 1
        }
        
        self.gameState = GameState.scored
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min: CGFloat, max: CGFloat) -> CGFloat {
        return random() * (max - min) + min
    }

    //MARK: UIInteraction
    
    func touchUp(atPoint pos : CGPoint) {
        
        if (self.gameState == GameState.start || self.gameState == GameState.scored) {
            self.initialPositions()
            self.startBall()
        }
    }
    
    func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            
            let position = touch.location(in: self.scene!)
            if position.x >= self.size.width/2.0 {
                self.selectedPlayer = PlayerType.playerRight
            } else {
                self.selectedPlayer = PlayerType.playerLeft
            }
        }
    }
    
    func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch in touches {
            let positionInScene = touch.location(in: self.scene!)
            let previousPosition = touch.previousLocation(in: self.scene!)
            let translation = CGPoint(x: positionInScene.x - previousPosition.x, y: positionInScene.y - previousPosition.y)
            
            panForTranslation(translation: translation)
        }
        
    }
    
    func updateUI() {
        
        self.scoreLabel?.text = String(format: "%d   |   %d", self.leftPlayerScore, self.rightPlayerScore)
        
        if (self.gameState == GameState.start || self.gameState == GameState.scored) {
            self.textLabel?.isHidden = false
            self.textLabel?.text = "Tap to start"
        } else {
            self.textLabel?.isHidden = true
        }

    }
}



