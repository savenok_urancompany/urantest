//
//  GameScene.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import SpriteKit
import GameplayKit

protocol UIInteraction {
    func touchUp(atPoint pos : CGPoint)
    func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?)
    func updateUI()
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    let gameLogic = GameLogic()
    
    override func didMove(to view: SKView) {
        
        backgroundColor = SKColor.white
        
        self.physicsWorld.gravity = CGVector(dx: 0.0, dy: 0.0)
        self.physicsWorld.contactDelegate = self
        
        self.removeAllChildren()
        
        self.gameLogic.sceneInitialSetup(size: self.size, scene: self)
        
        self.addChild(self.gameLogic.ball!)
        self.addChild(self.gameLogic.leftRaquet!)
        self.addChild(self.gameLogic.rightRaquet!)
        self.addChild(self.gameLogic.textLabel!)
        self.addChild(self.gameLogic.scoreLabel!)
        self.addChild(self.gameLogic.centralBorder!)
        for node in self.gameLogic.borders! {
            self.addChild(node)
        }
    }
    
        
    func touchUp(atPoint pos : CGPoint) {
        
        self.gameLogic.touchUp(atPoint: pos)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.gameLogic.touchesBegan(touches, with: event)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.gameLogic.touchesMoved(touches, with: event)
    }
    
        
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        for t in touches { self.touchUp(atPoint: t.location(in: self)) }
    }
    
    
    override func update(_ currentTime: TimeInterval) {
        self.gameLogic.updateUI()
    }
    
    func didBegin(_ contact: SKPhysicsContact){
        
        var bodyA: SKPhysicsBody
        var bodyB: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            bodyA = contact.bodyA
            bodyB = contact.bodyB
        } else {
            bodyA = contact.bodyB
            bodyB = contact.bodyA
        }
        
        let collision = self.gameLogic.processContact(bodyA: bodyA, bodyB: bodyB)
        
        self.gameLogic.processCollision(collision: collision)
    }
    
}
