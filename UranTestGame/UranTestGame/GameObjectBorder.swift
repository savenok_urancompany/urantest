//
//  GameObjectBorder.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

class GameObjectBorder: SKSpriteNode {
    
    convenience init(size: CGSize, textureName: String, collisionType: UInt32) {
        
        self.init(imageNamed: textureName)
        self.size = size
        self.physicsBody = SKPhysicsBody(rectangleOf: size)
        self.physicsBody?.isDynamic = true
        self.physicsBody?.categoryBitMask = collisionType
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
        self.physicsBody?.collisionBitMask = collisionType
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
}
