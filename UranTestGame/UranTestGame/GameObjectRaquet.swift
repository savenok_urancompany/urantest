//
//  GameObjectRaquet.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

class GameObjectRaquet: SKSpriteNode {
    
    convenience init(size: CGSize, textureName: String) {
        
        self.init(imageNamed: textureName)
        self.size = size
        self.physicsBody = SKPhysicsBody(rectangleOf: size)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = PhysicsCategory.Player
        self.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
        self.physicsBody?.collisionBitMask = PhysicsCategory.Player
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
}
