//
//  GameObjectBall.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

class GameObjectBall: SKSpriteNode {
    
    convenience init(size: CGSize, textureName: String) {
        
        self.init(imageNamed: textureName)
        self.size = size
        self.physicsBody = SKPhysicsBody(circleOfRadius: size.width/2)
        self.physicsBody?.isDynamic = true
        self.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
        self.physicsBody?.contactTestBitMask = PhysicsCategory.All
        self.physicsBody?.collisionBitMask = PhysicsCategory.Projectile
        self.physicsBody?.linearDamping = 0
        self.physicsBody?.usesPreciseCollisionDetection = true
    }
}
