//
//  UranTestGameTests.swift
//  UranTestGameTests
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import XCTest
import GameplayKit
@testable import UranTestGame

// All collision/decision logic in game is made by built-in physics engine and tied to rendering of physics engine, so it can be unit tested only with extensive usage of mock objects. I had time only for basic mockups for collisions.

class UranTestGameTests: XCTestCase {
    
    let screenSize = CGSize(width: 320, height: 568)
    var gameLogic: GameLogic!
    var scene: GameScene!
    
    var mock1: GameObjectBall!
    
    var mock2: GameObjectBall!
    
    var mock3: GameObjectRaquet!
    
    override func setUp() {
        super.setUp()
        self.gameLogic = GameLogic()
        self.scene = GameScene(size: self.screenSize)
        self.gameLogic.sceneInitialSetup(size: self.screenSize, scene: self.scene)

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testVelocityMirroringZero() {
        
        let velocity = CGVector(dx:0, dy:0)
        let mirroredVelocity = self.gameLogic.mirrorVelocity(velocity: velocity, horizontal: true)
        
        XCTAssert(mirroredVelocity == velocity)
    }
    
    func testVelocityMirroring() {
        
        let velocity = CGVector(dx:150, dy:30)
        let mirroredVelocity = self.gameLogic.mirrorVelocity(velocity: velocity, horizontal: true)
        
        XCTAssert(mirroredVelocity == CGVector(dx:-150, dy:30))
    }
    
    func testCollisionNone() {
        
        self.mock1 = GameObjectBall(size: CGSize(width: GameConstants.GameObjects.Ball.width,
                                                 height: GameConstants.GameObjects.Ball.height),
                                    textureName: GameConstants.GameObjects.Ball.texture)
        self.mock2 = GameObjectBall(size: CGSize(width: GameConstants.GameObjects.Ball.width,
                                                 height: GameConstants.GameObjects.Ball.height),
                                    textureName: GameConstants.GameObjects.Ball.texture)
        
        let collision = self.gameLogic.processContact(bodyA: mock1.physicsBody!, bodyB: mock2.physicsBody!)
        XCTAssert(collision == CollisionType.None)
    }
    
    func testCollisionRaquet() {
        
        self.mock2 = GameObjectBall(size: CGSize(width: GameConstants.GameObjects.Ball.width,
                                                 height: GameConstants.GameObjects.Ball.height),
                                    textureName: GameConstants.GameObjects.Ball.texture)
        
        self.mock3 = GameObjectRaquet(size: CGSize(width: GameConstants.GameObjects.Raquet.width,
                                                   height: GameConstants.GameObjects.Raquet.height),
                                      textureName: GameConstants.GameObjects.Raquet.texture)
        
        let collision = self.gameLogic.processContact(bodyA: mock2.physicsBody!, bodyB: mock3.physicsBody!)
        XCTAssert(collision == CollisionType.Raquet)
    }
    
    func testPlayerScore() {
        
        self.gameLogic.scoreGoal(player: PlayerType.playerLeft)
        XCTAssert(self.gameLogic.leftPlayerScore == 1)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
