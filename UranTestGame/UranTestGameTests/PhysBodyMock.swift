//
//  PhysBodyMock.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

class PhysBodyMock: SKPhysicsBody {
    
    func setupMockForType(type: UInt32) {
        
        self.categoryBitMask = type
        self.contactTestBitMask = PhysicsCategory.All
        self.collisionBitMask = type
    }
}
