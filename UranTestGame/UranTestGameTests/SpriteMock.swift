//
//  SpriteMock.swift
//  UranTestGame
//
//  Created by User on 10/6/16.
//  Copyright © 2016 User. All rights reserved.
//

import Foundation
import GameplayKit

class SpriteMock: SKSpriteNode {
    
    func setupPhysicsMockForType(type: UInt32) {
        
        self.physicsBody = SKPhysicsBody(circleOfRadius: 20.0)
        self.physicsBody?.isDynamic = false
        self.physicsBody?.categoryBitMask = type
        self.physicsBody?.contactTestBitMask = PhysicsCategory.All
        self.physicsBody?.collisionBitMask = type
    }
}
